import './App.css';
import React from 'react';
import ComponentName from './ComponentName';

function App() {
  return (
    <div className='App'>
      <ComponentName />
    </div>
  );
}

export default App;
