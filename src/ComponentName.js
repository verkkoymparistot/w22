import React from "react";


const ComponentName = (props) => {
    const country = props.country || "Finland" ; 
    return (
    <div className="comp">
        <h1>Hello {country}!</h1>
    </div>
    );
}

  export default ComponentName;
  